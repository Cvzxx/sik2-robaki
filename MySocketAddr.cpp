//
// Created by pawel on 23.08.2021.
//


#include "MySocketAddr.h"



MySocketAddr::MySocketAddr() {
    length = sizeof(address);
}

sockaddr_storage *MySocketAddr::get_addrr() {
    return &address;
}

socklen_t &MySocketAddr::len() {
    return length;
}


socklen_t *MySocketAddr::len_ptr() {
    return &length;
}

socklen_t MySocketAddr::len() const {
    return length;
}

sockaddr_storage const *MySocketAddr::get_addr() const {
    return &address;
}

bool MySocketAddr::operator<(const MySocketAddr &rhs) const {
    return memcmp(rhs.get_addr(), get_addr(), std::max(len(), rhs.len())) < 0;
}

bool MySocketAddr::operator>(const MySocketAddr &rhs) const {
    return rhs < *this;
}

bool MySocketAddr::operator<=(const MySocketAddr &rhs) const {
    return !(rhs < *this);
}

bool MySocketAddr::operator>=(const MySocketAddr &rhs) const {
    return !(*this < rhs);
}



