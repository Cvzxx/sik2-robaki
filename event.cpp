//
// Created by pawel on 22.08.2021.
//


#include "event.h"



uint32_t event::getLen() const {
    return len;
}



event::event(uint32_t len) : len(len) {}

void
event::create_new_game_event(uint32_t event_no, uint32_t maxx, uint32_t maxy,
                             const std::vector<std::string>& vec) {
    char event_type = 0;
    uint32_t curr_len = 0;
    len += sizeof(curr_len);

    add_4bytes(event_no);
    add_1bytes(event_type);
    add_4bytes(maxx);
    add_4bytes(maxy);
    add_strings(vec);

    curr_len = len - sizeof(curr_len);
    len = 0;
    add_4bytes(curr_len);//add len data at beg
    len = curr_len + sizeof(curr_len);

    //add crc32
    add_crc32();
}

void event::create_pixel_event(uint32_t event_no, char player_no, double x,
                               double y) {
    char event_type = 1;
    uint32_t curr_len = 0;
    len += sizeof(curr_len);
    auto temp_x = static_cast <uint32_t> (std::floor(x));
    auto temp_y = static_cast <uint32_t> (std::floor(y));

    add_4bytes(event_no);
    add_1bytes(event_type);
    add_1bytes(player_no);
    add_4bytes(temp_x);
    add_4bytes(temp_y);

    curr_len = len - sizeof(curr_len);
    len = 0;
    add_4bytes(curr_len);//add len data at beg
    len = curr_len + sizeof(curr_len);

    //add crc32
    add_crc32();
}

void event::create_player_eliminated_event(uint32_t event_no, char player_no) {
    char event_type = 2;
    uint32_t curr_len = 0;
    len += sizeof(curr_len);

    add_4bytes(event_no);
    add_1bytes(event_type);
    add_1bytes(player_no);

    curr_len = len - sizeof(curr_len);
    len = 0;
    add_4bytes(curr_len);//add len data at beg
    len = curr_len + sizeof(curr_len);

    //add crc32
    add_crc32();
}

void event::create_game_over_event(uint32_t event_no) {
    char event_type = 3;
    uint32_t curr_len = 0;
    len += sizeof(curr_len);

    add_4bytes(event_no);
    add_1bytes(event_type);

    curr_len = len - sizeof(curr_len);
    len = 0;
    add_4bytes(curr_len);//add len data at beg
    len = curr_len + sizeof(curr_len);

    //add crc32
    add_crc32();
}

void event::add_crc32() {
    uint32_t temp_crc32 = crc32(0, (Bytef *) buffer, len);
    uint32_t temp = htobe32(temp_crc32);
    memcpy(&buffer[len], &temp, sizeof(temp));
    len += sizeof(temp);
}

void event::add_4bytes(uint32_t number) {
    uint32_t temp = htobe32(number);
    memcpy(&buffer[len], &temp, sizeof(temp));
    len += sizeof(temp);
}

void event::add_1bytes(char byte) {
    char temp = byte;
    memcpy(&buffer[len], &temp, sizeof(temp));
    len += sizeof(temp);
}

void event::add_strings(const std::vector<std::string> &vec) {
    std::string temp_str;
    for(const auto& str : vec){
        add_string(str);
    }
}

void event::add_string(const std::string& str) {
    std::string temp_str = str + '\0';
    size_t temp_len = str.length() + 1;
    memcpy(&buffer[len], temp_str.c_str(), temp_len);
    len += temp_len;
}

char *event::get_buffer() {
    return buffer;
}

