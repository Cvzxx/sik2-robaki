//
// Created by pawel on 22.08.2021.
//


#include "player.h"

bool valid_name(const std::string& player_name) {
    if (player_name.size() > 20)
        return false;

    for (auto c : player_name) {
        if (c < 36 || c > 126)
            return false;
    }

    return true;
}

player::player(uint64_t sessionId, char turnDirection,
               uint32_t nextExpectedEventNo, std::string playerName)
        : session_id(sessionId), turn_direction(turnDirection),
          next_expected_event_no(nextExpectedEventNo),
          player_name(std::move(playerName)) {}

uint64_t player::getSessionId() const {
    return session_id;
}

char player::getTurnDirection() const {
    return turn_direction;
}

uint32_t player::getNextExpectedEventNo() const {
    return next_expected_event_no;
}

const std::string &player::getPlayerName() const {
    return player_name;
}

bool player::isReady() const {
    return is_ready;
}

double player::getX() const {
    return x;
}

double player::getY() const {
    return y;
}

void player::setIsReady(bool isReady) {
    is_ready = isReady;
}

bool player::isDisconnected() const {
    return is_disconnected;
}

void player::setIsDisconnected(bool isDisconnected) {
    is_disconnected = isDisconnected;
}

void player::setSessionId(uint64_t sessionId) {
    session_id = sessionId;
}

void player::setTurnDirection(char turnDirection) {
    turn_direction = turnDirection;
}

void player::setNextExpectedEventNo(uint32_t nextExpectedEventNo) {
    next_expected_event_no = nextExpectedEventNo;
}

void player::setX(double x_) {
   x = x_;
}

void player::setY(double y_) {
    y = y_;
}

void player::setCurrentDirection(int currentDirection) {
    current_direction = currentDirection;
}

bool player::is_valid_name() {
    return valid_name(this->player_name);
}

std::chrono::high_resolution_clock::time_point player::getLastMess() const {
    return last_mess;
}

void player::setLastMess(std::chrono::high_resolution_clock::time_point lastMess) {
    last_mess = lastMess;
}

int player::getCurrentDirection() const {
    return current_direction;
}

bool player::isAlive() const {
    return is_alive;
}

void player::setIsAlive(bool isAlive) {
    is_alive = isAlive;
}

std::pair<double, double> player::move_player_one_pixel() const {
    double next_x = x;
    double next_y = y;
    next_x += cos(static_cast<double>(current_direction) * M_PI / 180.0);
    next_y += sin(static_cast<double>(current_direction) * M_PI/180.0);

    return std::make_pair(next_x, next_y);
}

bool player::cmp_cords(double new_x, double new_y) const {
    auto temp_x = static_cast <uint32_t> (std::floor(x));
    auto temp_y = static_cast <uint32_t> (std::floor(y));
    auto temp_new_x = static_cast <uint32_t> (std::floor(new_x));
    auto temp_new_y = static_cast <uint32_t> (std::floor(new_y));

    return (temp_x == temp_new_x && temp_y == temp_new_y);
}

bool player::is_valid() {
    if(!is_valid_name()) {
        std::cerr << "wrong player_name\n";
        return false;
    }
    if(!(turn_direction == 0 || turn_direction == 1 || turn_direction == 2)){
            std::cerr << "wrong turn_direction\n";
            return false;
    }


    return true;
}

char player::getLastNotZero() const {
    return last_not_zero;
}

void player::setLastNotZero(char lastNotZero) {
    last_not_zero = lastNotZero;
}




