//
// Created by pawel on 26.08.2021.
//

#ifndef SIK2_CLIENT_H
#define SIK2_CLIENT_H

#include <memory>
#include <chrono>
#include <poll.h>
#include <queue>
#include <netdb.h>
#include <zlib.h>
#include <utility>
#include <sstream>
#include <zconf.h>
#include <cstdint>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <cstdio>
#include <thread>
#include <netinet/tcp.h>
#include <vector>
#include <algorithm>
#include <set>
#include "ClientToServerMess.h"
#define L 2

class client {
public:
    bool
    set_client(const std::string& server, uint32_t server_port_num, const std::string& gui,
               uint32_t
               gui_port_num);

    void create_client(std::string player_name_);

    void run();

private:

    void set_server(const std::string& server, uint32_t server_port_num);

    void set_gui(const std::string& gui, uint32_t
    gui_port_num);

    //network data
    int server_socket;
    int gui_socket;
    struct pollfd client_poll[L];

    void reset_poll();

    void set_poll();

    void create_poll();

    void handle_mess_from_server();

    void handle_mess_from_gui();

    void try_send_to_server();

    void try_send_to_gui();

    void create_mess_to_send_to_server();

    void try_read_mess_from_server(char *mess_buff, ssize_t len_);

    bool check_event_no(uint32_t new_event_no) const;

    const size_t max_mess_from_server_size = 550;

    //player state
    std::string player_name;
    uint64_t session_id;
    char turn_direction;
    uint32_t next_expected_event_no;
    uint32_t curr_game_id;

    //game stare
    uint32_t board_width;
    uint32_t board_height;

    std::string
    encode_player_names(char *datagram_start, ssize_t len, ssize_t *pos,
                        bool *flag, uint32_t datagram_len);

    std::vector<std::string> players_names;
    std::queue<std::string> gui_mess_history;
    std::queue<std::shared_ptr<ClientToServerMessHandler>> server_mess_history;
    bool wes_serv_informed = false;

    std::chrono::high_resolution_clock::time_point last_mess_to_server;
};


#endif //SIK2_CLIENT_H
