//
// Created by pawel on 24.08.2021.
//

#ifndef SIK2_MYGENERATOR_H
#define SIK2_MYGENERATOR_H

#include <iostream>
class MyGenerator {
private:
    uint32_t seed;
    uint64_t scalar = 279410273;
    uint64_t rest = 4294967291;
public:
    uint32_t my_rand();

    void setSeed(uint32_t seed);
};


#endif //SIK2_MYGENERATOR_H
