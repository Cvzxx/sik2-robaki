//
// Created by pawel on 23.08.2021.
//

#include "ClientToServerMess.h"



uint64_t ClientToServerMess::getSessionId() const {
    return session_id;
}

char ClientToServerMess::getTurnDirection() const {
    return turn_direction;
}

uint32_t ClientToServerMess::getNextExpectedEventNo() const {
    return next_expected_event_no;
}

const std::string &ClientToServerMess::getPlayerName() const {
    return player_name;
}

bool ClientToServerMess::create_parser(char *buffer, ssize_t len) {
    if (buffer == nullptr || len == 0) {
        std::cerr << "problem with reading datagram\n";
        return false;
    }

    ssize_t pos = 0;
    if (!encode_session_id(buffer, len, &pos)) {
        return false;
    }

    if (!encode_turn_direction(buffer, len, &pos)) {
        return false;
    }

    if (!encode_next_expected_event_no(buffer, len, &pos)) {
        return false;
    }

    if (!encode_player_name(buffer, len, &pos)) {
        return false;
    }

    return true;
}

bool
ClientToServerMess::encode_session_id(char *buffer, ssize_t len, ssize_t *pos) {
    if (len - *pos < (ssize_t) sizeof(session_id)) {
        std::cerr << "datagram can not contains session_id\n";
        return false;
    }

    uint64_t temp = *(uint64_t *) &buffer[*pos];
    *pos += sizeof(session_id);
    session_id = be64toh(temp);

    return true;
}

bool ClientToServerMess::encode_turn_direction(char *buffer, ssize_t len,
                                               ssize_t *pos) {

    if (len - *pos < (ssize_t) sizeof(session_id)) {
        std::cerr << "datagram can not contains turn_direction\n";
        return false;
    }
    char temp = *(char *) &buffer[*pos];
    *pos += sizeof(turn_direction);
    turn_direction = temp;

    if (!(turn_direction == 0 || turn_direction == 1 || turn_direction == 2)) {
        std::cerr << "err, wrong turn_direction value\n";
        return false;
    }

    return true;
}

bool
ClientToServerMess::encode_next_expected_event_no(char *buffer, ssize_t len,
                                                  ssize_t *pos) {

    if (len - *pos < (ssize_t) sizeof(session_id)) {
        std::cerr << "datagram can not contains next_expected_event_no\n";
        return false;
    }
    uint32_t temp = *(uint32_t *) &buffer[*pos];
    *pos += sizeof(next_expected_event_no);
    next_expected_event_no = be32toh(temp);
    return true;
}

bool
ClientToServerMess::encode_player_name(char *buffer, ssize_t len,
                                       ssize_t *pos) {
    if (len - *pos <= 0) {
        std::cerr << "datagram can not contains player_name\n";
        return false;
    }

//    if(buffer[len - 1] != '\0'){
//        buffer[len] = '\0';
//    }

    player_name = std::string(&buffer[*pos]);
    return true;
}

void
ClientToServerMess::code_session_id() {
    uint64_t temp = htobe64(session_id);
    session_id = temp;
}


void ClientToServerMess::code_next_expected_event_no() {
    uint32_t temp = htobe32(next_expected_event_no);
    next_expected_event_no = temp;
}



void ClientToServerMess::create_sender(){

    code_session_id();
    code_next_expected_event_no();
}

void ClientToServerMess::setSessionId(uint64_t sessionId) {
    session_id = sessionId;
}

void ClientToServerMess::setTurnDirection(char turnDirection) {
    turn_direction = turnDirection;
}

void ClientToServerMess::setNextExpectedEventNo(uint32_t nextExpectedEventNo) {
    next_expected_event_no = nextExpectedEventNo;
}

void ClientToServerMess::setPlayerName(const std::string &playerName) {
    player_name = playerName;
}




ServerToClientMessHandler::ServerToClientMessHandler(uint32_t len) : len(len) {}

void ServerToClientMessHandler::create_mess( char *mess,
                                            uint32_t mess_size) {

    memcpy(&buffer[len], mess, mess_size);
    len += mess_size;
}

char *ServerToClientMessHandler::get_buffer() {
    return buffer;
}

uint32_t ServerToClientMessHandler::get_len() {
    return len;
}

ClientToServerMessHandler::ClientToServerMessHandler(uint32_t len) : len(len) {}

void
ClientToServerMessHandler::create_mess(ClientToServerMess mess) {

    len = 0;

    uint64_t session_id = mess.getSessionId();
    uint32_t event_no = mess.getNextExpectedEventNo();
    char turn = mess.getTurnDirection();
    std::string temp_str = mess.getPlayerName() + '\0';
    size_t temp_len = mess.getPlayerName().length() + 1;


    memcpy(&buffer[len], &session_id, sizeof(session_id));
    len += sizeof(session_id);

    memcpy(&buffer[len], &turn, sizeof(char ));
    len += sizeof(char);
//
    memcpy(&buffer[len], &event_no, sizeof(event_no ));
    len += sizeof(event_no);

    memcpy(&buffer[len], temp_str.c_str(), temp_len);
    len += temp_len;

}


char *ClientToServerMessHandler::get_buffer() {
    return buffer;
}


uint32_t ClientToServerMessHandler::get_len() {
    return len;
}

