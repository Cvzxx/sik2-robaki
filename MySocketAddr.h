//
// Created by pawel on 23.08.2021.
//

#ifndef SIK2_MYSOCKETADDR_H
#define SIK2_MYSOCKETADDR_H
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <netdb.h>
#include <sys/socket.h>
#include <cstring>

class MySocketAddr {
public:
    MySocketAddr();
    socklen_t & len();
    sockaddr_storage* get_addrr();
    socklen_t len() const;
    sockaddr_storage const* get_addr() const;
    socklen_t *len_ptr();

private:
    sockaddr_storage address;
    socklen_t length;

public:
    bool operator<(const MySocketAddr &rhs) const;

    bool operator>(const MySocketAddr &rhs) const;

    bool operator<=(const MySocketAddr &rhs) const;

    bool operator>=(const MySocketAddr &rhs) const;
};


#endif //SIK2_MYSOCKETADDR_H
