//
// Created by pawel on 22.08.2021.
//

#ifndef SIK2_SERVER_H
#define SIK2_SERVER_H

#include <iostream>
#include <ctime>
#include <poll.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <map>
#include <vector>
#include <memory>
#include <netinet/in.h>
#include <set>
#include <queue>
#include <chrono>
#include <string>
#include <cstring>
#include <chrono>
#include <utility>
#include <cstdint>
#include <cstdlib>

#include "Game.h"
#include "MySocketAddr.h"

#define N 1

class server {
public:
    explicit server(uint32_t port_num);

    void run();

    void set_server();

    void
    set_game(uint32_t turningSpeed, uint32_t roundsPerSec, uint32_t boardWidth,
             uint32_t boardHeight, uint32_t seed);

    bool handle_mess_from_client();
    void handle_mess_to_client();
    void try_start_game();
    void new_session();
    void try_start_round();
    void new_round();


private:
    //GAMESTATE
    void send_events(const MySocketAddr &player_address,
                     uint32_t next_expected_event_no, uint32_t game_id);

    void send_event_to_all( uint32_t game_id);

    void send_to_all();
    void reset_poll();
    void set_poll();
    void create_poll();
    void check_active_players();

    uint64_t checkout_time = 500 * 1000;
    uint32_t port_num;
    uint16_t max_client_mess_len = 8 + 1 + 4 + 20;
    int listening_socket;
    struct sockaddr_in6 server_address;
    struct pollfd server_poll[N];
    std::shared_ptr<Game> game_state;
    std::vector<std::shared_ptr<event>> events_history;
    std::map<MySocketAddr, std::shared_ptr<player>> clients;
    std::queue<std::pair<MySocketAddr, std::shared_ptr<ServerToClientMessHandler>>> events_to_send ;
    std::chrono::high_resolution_clock::time_point last_checkout;
};


#endif //SIK2_SERVER_H
