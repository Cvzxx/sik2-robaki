//
// Created by pawel on 22.08.2021.
//

#include "server.h"

bool valid_params(uint32_t turning_speed, uint32_t rounds_per_sec,
                  uint32_t board_width, uint32_t board_height,
                  uint32_t port_num) {
    if (port_num > 65535) {
        std::cerr << "wrong port num\n";
        return false;
    }

    if (rounds_per_sec < 1 || rounds_per_sec > 250) {
        std::cerr
                << "wrong rounds_per_sec value (should be in range [1, 250])\n";
        return false;
    }

    if (turning_speed < 1 || turning_speed > 359) {
        std::cerr << "wrong turning speed value (should be in range [0, 359]\n";
        return false;
    }

    if (board_width < 16 || board_width > 4096) {
        std::cerr << "wrong board_width value (should be in range [16, 4096]\n";
        return false;
    }

    if (board_height < 16 || board_height > 4096) {
        std::cerr
                << "wrong board_height value (should be in range [16, 4096]\n";
        return false;
    }


    return true;
}

bool is_num(const char arg[])
{
    if (arg == NULL)
        return false;

    char *ptr;
    strtol(arg, &ptr, 10);
    return *ptr == 0;
}
int main(int argc, char *argv[]) {

    int opt;
    auto rng = static_cast<uint32_t>(time(nullptr));
    uint32_t turning_speed = 6;
    uint32_t rounds_per_sec = 50;
    uint32_t board_width = 640;
    uint32_t board_height = 480;
    uint32_t port_num = 2021;
    uint32_t val;
    while ((opt = getopt(argc, argv, "p:s:t:v:w:h:")) != -1) {
        if(!is_num(optarg)){
            std::cerr << "WRONG PARAMS\n";
            exit(1);
        }
        val = static_cast<uint32_t>(std::stoul(optarg));


        switch (opt) {
            case 'p':
                port_num = val;
                break;
            case 's':
                rng = val;
                break;
            case 't':
                turning_speed = val;
                break;
            case 'v':
                rounds_per_sec = val;
                break;
            case 'w':
                board_width = val;
                break;
            case 'h':
                board_height = val;
                break;
            default:
                std::cerr << "err\n";
                exit(1);
        }
    }

    if (optind < argc) {
        std::cerr << "WRONG PARAMS\n";
        exit(1);
    }
    std::cout << " [-p " << port_num << " ] [-s " << rng << " ] [-t "
              << turning_speed << " ] [-v "
              << rounds_per_sec << " ] [-w "
              << board_width << " ] [-h "
              << board_height << " ]\n";

    if (!valid_params(turning_speed, rounds_per_sec, board_width, board_height,
                      port_num)) {
        return 1;
    }
    auto my_server = std::make_shared<server>(port_num);
    my_server->set_server();
    my_server->set_game(turning_speed, rounds_per_sec, board_width,
                        board_height, rng);
    my_server->run();
    return 0;
}