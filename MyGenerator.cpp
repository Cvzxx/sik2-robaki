//
// Created by pawel on 24.08.2021.
//

#include "MyGenerator.h"

void MyGenerator::setSeed(uint32_t seed_) {
    seed = seed_;
}

uint32_t MyGenerator::my_rand() {
    uint32_t temp = seed;
    uint64_t xd = scalar * temp;
    xd %= rest;
    seed = xd;

    return temp;
}
