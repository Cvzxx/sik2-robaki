//
// Created by pawel on 22.08.2021.
//

#ifndef SIK2_EVENT_H
#define SIK2_EVENT_H

#include <iostream>
#include <cstring>
#include <vector>
#include <cmath>
#include <zlib.h>
class event {
public:
    explicit event(uint32_t len);

    [[nodiscard]] uint32_t getLen() const;
    void create_new_game_event(uint32_t event_no, uint32_t maxx, uint32_t maxy, const std::vector<std::string>& vec);
    void create_pixel_event(uint32_t event_no, char player_no, double x, double y);
    void create_player_eliminated_event(uint32_t event_no, char player_no);
    void create_game_over_event(uint32_t event_no);
    char *get_buffer();
private:
    char buffer[546];
    uint32_t len;
    void add_4bytes(uint32_t number);
    void add_1bytes(char byte);
    void add_strings(const std::vector<std::string>& vec);
    void add_string(const std::string& str);
    void add_crc32();
};


#endif //SIK2_EVENT_H
