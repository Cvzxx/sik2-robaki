//
// Created by pawel on 26.08.2021.
//

#include "client.h"
#include <unistd.h>

bool valid_params(std::string player_name, uint32_t port_server_num,
                  uint32_t port_user_num) {
    if ( port_server_num > 65535) {
        std::cerr << "wrong port_server_num\n";
        return false;
    }

    if ( port_user_num > 65535) {
        std::cerr << "wrong port_server_num\n";
        return false;
    }

    if (player_name.size() > 20) {
        std::cerr << "wrong player_name\n";
        return false;
    }

    for (auto c: player_name) {
        if (c < 36 || c > 126) {
            std::cerr << "wrong player_name\n";
            return false;
        }
    }

    return true;
}
bool is_num(const char arg[])
{
    if (arg == NULL)
        return false;

    char *ptr;
    strtol(arg, &ptr, 10);
    return *ptr == 0;
}
int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cerr << "not enough args\n";
        exit(1);
    }
    int opt;

    std::string server_infos = argv[1];
    std::string player_name;
    std::string gui_infos = "localhost";
    uint32_t port_server_num = 2021;
    uint32_t port_user_num = 20210;
    uint32_t val;
    while ((opt = getopt(argc, argv, "n:p:i:r:")) != -1) {

        switch (opt) {
            case 'n':
                player_name = std::string(optarg);
                break;
            case 'p':
                if(!is_num(optarg)){
                    std::cerr << "WRONG PARAMS\n";
                    exit(1);
                }
                val = static_cast<uint32_t>(std::stoul(optarg));
                port_server_num = val;
                break;
            case 'i':
                gui_infos = std::string(optarg);
                break;
            case 'r':
                if(!is_num(optarg)){
                    std::cerr << "WRONG PARAMS\n";
                    exit(1);
                }
                val = static_cast<uint32_t>(std::stoul(optarg));
                port_user_num = val;
                break;
            default:
                std::cerr << "err\n";
                return 1;
        }
    }

    if (optind < argc - 1) {
        std::cerr << "WRONG PARAMS\n";
        exit(1);
    }
    std::cout << server_infos << " [-n " << player_name << " ] [-p "
              << port_server_num << " ] [-i "
              << gui_infos << " ] [-r "
              << port_user_num << "]\n";

    if (!valid_params(player_name, port_server_num, port_user_num)) {
        return 1;
    }

    //getaddrinfo sprawdzi validacej adresow

    auto my_client = std::make_shared<client>();
    if(!my_client->set_client(server_infos, port_server_num, gui_infos, port_user_num)){

        return 1;
    }

    my_client->create_client(player_name);
    my_client->run();

    return 0;
}