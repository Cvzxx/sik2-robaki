//
// Created by pawel on 22.08.2021.
//

#ifndef SIK2_PLAYER_H
#define SIK2_PLAYER_H

#include <iostream>
#include <chrono>
#include <utility>
#include <ctime>
#include <cmath>
class player {
private:
    uint64_t session_id;
    char turn_direction;
    char last_not_zero;
public:
    char getLastNotZero() const;

    void setLastNotZero(char lastNotZero);

private:
    uint32_t next_expected_event_no;
    std::string player_name;

    std::chrono::high_resolution_clock::time_point last_mess;

    bool is_ready = false;
    bool is_disconnected = false;
    //during game params

    double x;
    double y;
    int current_direction;
    bool is_alive = false;
public:
    bool cmp_cords(double new_x, double new_y) const;
    std::pair<double, double> move_player_one_pixel() const;
    void setIsReady(bool isReady);

    [[nodiscard]] bool isAlive() const;

    bool is_valid();
    void setIsAlive(bool isAlive);

    [[nodiscard]] uint64_t getSessionId() const;

    [[nodiscard]] char getTurnDirection() const;

    [[nodiscard]] std::chrono::high_resolution_clock::time_point getLastMess() const;
    void setLastMess(std::chrono::high_resolution_clock::time_point lastMess);
    void setSessionId(uint64_t sessionId);
    void setTurnDirection(char turnDirection);
    void setNextExpectedEventNo(uint32_t nextExpectedEventNo);


    void setX(double x);

    void setY(double y);

    [[nodiscard]] int getCurrentDirection() const;

    void setCurrentDirection(int currentDirection);

    [[nodiscard]] uint32_t getNextExpectedEventNo() const;

    [[nodiscard]] const std::string &getPlayerName() const;

    [[nodiscard]] bool isReady() const;

    [[nodiscard]] double getX() const;

    [[nodiscard]] double getY() const;

    [[nodiscard]] bool isDisconnected() const;

    void setIsDisconnected(bool isDisconnected);


    bool is_valid_name();

public:
    player(uint64_t sessionId, char turnDirection, uint32_t nextExpectedEventNo,
           std::string playerName);

};


bool valid_name(const std::string& player_name);

#endif //SIK2_PLAYER_H
