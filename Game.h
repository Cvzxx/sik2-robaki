//
// Created by pawel on 24.08.2021.
//

#ifndef SIK2_GAME_H
#define SIK2_GAME_H

#include <memory>
#include <string>
#include <algorithm>
#include "player.h"
#include "MyGenerator.h"
#include "ClientToServerMess.h"
#include "event.h"

//add checker limits to game params
class Game {
private:
    MyGenerator my_gen;
    //games info
    uint32_t turning_speed;
    uint32_t rounds_per_sec;
    uint32_t board_width;
    uint32_t board_height;
    uint32_t game_id;

    std::vector<std::shared_ptr<player>> spectators;
    std::vector<std::shared_ptr<player>> players;

    //game stats
    std::chrono::high_resolution_clock::time_point last_round;
    bool session_live;
    uint32_t alive_players;
    std::vector<std::shared_ptr<player>> active_players;
    std::vector<std::vector<uint32_t>> board;
public:
    uint32_t getGameId() const;

private:
    void clear_board();
    void find_active_players();

public:
    [[nodiscard]] uint32_t getRoundsPerSec() const;

private:
    void sort_player();
    bool check_cords_range(uint32_t x, uint32_t y);
    bool check_pixel(double x, double y);
    void eat_pixel(double x, double y);
public:
    size_t players_size();
    size_t num_players_ready();
    void set_last_round(std::chrono::high_resolution_clock::time_point last_round_time);
    std::chrono::high_resolution_clock::time_point get_last_round();
    void make_all_unready();
     bool is_session_live() ;
    void erase_inactive_players(std::string rem_name_player) ;
    bool handle_existing_player(std::shared_ptr<player> ext_player, ClientToServerMess mess);
    bool handle_new_player(std::shared_ptr<player> new_player);
    Game(uint32_t turningSpeed, uint32_t roundsPerSec, uint32_t boardWidth,
         uint32_t boardHeigth, uint32_t seed);


    void game_rand();
    uint32_t getBoardWidth() const;
    uint32_t getBoardHeigth() const;
    uint32_t get_rand();
    uint32_t getAlivePlayers() const;
    void new_game();
    std::vector<std::shared_ptr<event>> play_round(size_t nums);
    std::vector<std::shared_ptr<event>> init_players(size_t nums);
    std::vector<std::string> get_active_players_names();
};


#endif //SIK2_GAME_H
