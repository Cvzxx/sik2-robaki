//
// Created by pawel on 26.08.2021.
//


#include "client.h"

uint32_t encode_crc32(const char *buffer, ssize_t len, ssize_t pos,
                      bool *flag) {
    uint32_t temp;
    if (len - pos < (ssize_t) sizeof(temp)) {
        std::cerr << "datagram can not contains crc32\n";
        *flag = false;
        return 0;
    }

    temp = *(uint32_t *) &buffer[pos];
    uint32_t infos = be32toh(temp);

    return infos;
}

char encode_1bytes(const char *buffer, ssize_t len, ssize_t *pos,
                   bool *flag) {
    char temp;
    if (len - *pos <(ssize_t) sizeof(temp)) {
        std::cerr << "datagram can not contains 1 byte\n";
        *flag = false;
        return 0;
    }

    temp = *(char *) &buffer[*pos];
    *pos += sizeof(temp);

    return temp;
}

uint32_t encode_4bytes(const char *buffer, ssize_t len, ssize_t *pos,
                       bool *flag) {
    uint32_t temp;
    if (len - *pos < (ssize_t) sizeof(temp)) {
        std::cerr << "datagram can not contains 4 bytes\n";
        *flag = false;
        return 0;
    }

    temp = *(uint32_t *) &buffer[*pos];
    *pos += sizeof(temp);
    uint32_t infos = be32toh(temp);

    return infos;
}

bool client::set_client(const std::string& server, uint32_t server_port_num,
                        const std::string& gui, uint32_t
                        gui_port_num) {


    set_gui(gui, gui_port_num);
    set_server(server, server_port_num);

    return true;
}


void client::set_gui(const std::string& gui, uint32_t gui_port_num) {
    int s;
    struct addrinfo hints;
    struct addrinfo *result;

    memset(&hints, 0, sizeof(addrinfo));
    hints.ai_family = AF_UNSPEC;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
    hints.ai_protocol = IPPROTO_TCP;
    // hints.ai_flags = AI_PASSIVE;

    s = getaddrinfo(gui.c_str(), std::to_string(gui_port_num).c_str(), &hints, &result);

    if(s != 0) {
        std::cout << gui << " " << gui_port_num << std::endl;
        fprintf(stderr, "GUI getaddrinfo: %s\n", gai_strerror(s));
        exit(1);
    }

    gui_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if (gui_socket < 0) {
        std::cerr << "gui_socket\n";
        exit(1);
    }

    int xd;
    if ((xd = connect(gui_socket, result->ai_addr,result->ai_addrlen)) < 0) {
        std::cerr << "gui connection problem << " << xd << "\n";

        exit(1);
    }

    //turn of nagle
    int flag = 1;
    int re = setsockopt(gui_socket,
                        IPPROTO_TCP,
                        TCP_NODELAY,
                        (char *) &flag,
                        sizeof(int));
    if (re < 0) {
        std::cerr << "gui nagle turn off problem\n";
        exit(1);
    }

    freeaddrinfo(result);
}

void client::set_server(const std::string& server, uint32_t server_port_num) {
    int s;
    struct addrinfo hints;
    struct addrinfo *result;

    memset(&hints, 0, sizeof(addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = IPPROTO_UDP;
    hints.ai_flags = 0;
    hints.ai_addrlen = 0;
    hints.ai_addr = nullptr;
    hints.ai_canonname = nullptr;
    hints.ai_next = nullptr;

    s = getaddrinfo(server.c_str(), std::to_string(server_port_num).c_str(), &hints, &result);

    if(s != 0) {
        fprintf(stderr, "SERVER getaddrinfo: %s\n", gai_strerror(s));
        exit(1);
    }

    server_socket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

    if (gui_socket < 0) {
        std::cerr << "gui_socket\n";
        exit(1);
    }

    if ((connect(server_socket, result->ai_addr,result->ai_addrlen) < 0)) {
        std::cerr << "server connection problem\n";
        exit(1);
    }

    freeaddrinfo(result);
}

void client::create_client(std::string player_name_) {
    player_name = std::move(player_name_);
    auto elapse = std::chrono::high_resolution_clock::now().time_since_epoch();
    session_id = std::chrono::duration_cast<std::chrono::microseconds>(
            elapse).count();
    turn_direction = 0;
    next_expected_event_no = 0;


    create_poll();
}

void client::create_poll() {
    reset_poll();
    set_poll();
}

void client::reset_poll() {
    for (auto &i: client_poll) {
        i.fd = -1;
        i.events = POLLIN;
        i.revents = 0;
    }
}

void client::set_poll() {
    client_poll[0].fd = server_socket;
    client_poll[1].fd = gui_socket;
}


void client::run() {
    bool is_running = true;
    int ret;
    do {
        ret = poll(client_poll, 2, 1); // do poprawy

        if (ret == -1) {
            std::cerr << "client_poll poll() err\n";
            exit(1);
        } else {
            if (client_poll[0].revents & (POLLIN | POLLERR)) {

                handle_mess_from_server();
                client_poll[0].revents &= ~(POLLIN | POLLERR);
            }
            if (client_poll[1].revents & POLLIN) {
                handle_mess_from_gui();
            }
        }

        try_send_to_server();
        try_send_to_gui();
        create_mess_to_send_to_server();
    } while (is_running);
}

void client::handle_mess_from_server() {
    char buffer[max_mess_from_server_size + 2];
    ssize_t len;
    int flags;
    flags = 0;

    len = recv(server_socket, buffer, sizeof(buffer), flags);

    if (len < 0) {
        if (errno == 111) {
            std::cerr << "server lost connection\n";
            exit(1);
        } else {
            std::cerr
                    << "error on datagram from server socket\n"; // check errno
        }
    } else {
        if (len >= 0) {
            try_read_mess_from_server(buffer, len);
        }
    }
}

bool compare_crc32(uint32_t encoded_crc32, char *datagram, uint32_t len) {
    uint32_t temp_crc32 = crc32(0, (Bytef *) datagram, len);

    return (encoded_crc32 == temp_crc32);

}

void client::try_read_mess_from_server(char *mess_buff, ssize_t len_) {
    ssize_t pos = 0;
    while (true) {

        bool flag = true;
        uint32_t game_id = encode_4bytes(mess_buff, len_, &pos, &flag);
        //check flags;
        uint32_t datagram_len = encode_4bytes(mess_buff, len_, &pos, &flag);
        uint32_t crc32_ = encode_crc32(mess_buff, len_, datagram_len + 4 + 4,
                                       &flag);

        if (compare_crc32(crc32_, mess_buff + 4, datagram_len + 4)) {
            //rekord z poprawną sumą kontrolną, znanego typu, jednakże z bezsensownymi wartościami, powoduje zakończenie klienta z odpowiednim komunikatem i kodem wyjścia 1;
            //pomija się zdarzenia z poprawną sumą kontrolną oraz nieznanym typem.

            uint32_t event_no = encode_4bytes(mess_buff, len_, &pos, &flag);
            bool event_flag = true;
            if (check_event_no(event_no)) {
                next_expected_event_no = event_no + 1;
            } else {
                if (event_no > next_expected_event_no) {
                    break;
                } else {
                    event_flag = false;
                }
                //nie zgadza sie numer eventu na jaki czekam
            }

            char game_type = encode_1bytes(mess_buff, len_, &pos, &flag);

            if (game_id == curr_game_id || game_type == 0) {
                if (game_type == 0) {
                    curr_game_id = game_id;
                    uint32_t maxx = encode_4bytes(mess_buff, len_, &pos, &flag);
                    uint32_t maxy = encode_4bytes(mess_buff, len_, &pos, &flag);
                    board_width = maxx;
                    board_height = maxy;
                    std::string player_names_ = encode_player_names(
                            mess_buff+pos, len_, &pos, &flag,
                            datagram_len - (1 + 4 + 8));
                    pos += 4;

                    std::stringstream gui_mess;
                    gui_mess << "NEW_GAME " << maxx << " " << maxy << " "
                             << player_names_ << "\n";
                    gui_mess_history.push(gui_mess.str());
                    client_poll[1].events |= POLLOUT;
                } else if (game_type == 1) {
                    char player_no = encode_1bytes(mess_buff, len_, &pos,
                                                   &flag);
                    uint32_t x = encode_4bytes(mess_buff, len_, &pos, &flag);
                    uint32_t y = encode_4bytes(mess_buff, len_, &pos, &flag);

                    if (players_names.size() <= (size_t) player_no) {
                        std::cerr << "WRONG PLAYER NUM\n";
                        exit(1);
                    }

                    if (x >= board_width || y >= board_height) {
                        std::cerr << "WRONG PIXEL COORDS\n";
                        exit(1);

                    }
                    pos += 4;
                    std::stringstream gui_mess;
                    gui_mess << "PIXEL " << x << " " << y << " "
                             << players_names[(int) player_no] << "\n";
                    if (event_flag) {
                        gui_mess_history.push(gui_mess.str());
                        client_poll[1].events |= POLLOUT;
                    }

                } else if (game_type == 2) {
                    char player_no = encode_1bytes(mess_buff, len_, &pos,
                                                   &flag);

                    if (players_names.size() <= (size_t) player_no) {
                        std::cerr << "WRONG PLAYER NUM\n";
                        exit(1);
                    }

                    pos += 4;
                    std::stringstream gui_mess;
                    gui_mess << "PLAYER_ELIMINATED "
                             << players_names[(int) player_no] << "\n";
                    if (event_flag) {
                        gui_mess_history.push(gui_mess.str());
                        client_poll[1].events |= POLLOUT;
                    }
                } else if (game_type == 3) {
                    pos += 4;
                } else {
                    std::cerr << "WRONG EVENT TYPE SO SKIP\n";
                    pos -= (1 + 4);
                    pos += (datagram_len + 4);
                }
            } else {
                //jesli zly game_id to igonurjemy
                std::cerr << "WRONG GAME ID\n";
                pos -= (1 + 4);
                pos += (datagram_len + 4);
            }


        } else {
            break;
            //nie zgadza sie suma kontrola to trzeba przestac
        }

    }
}





std::string
client::encode_player_names(char *datagram_start, ssize_t len, ssize_t *pos,
                            bool *flag, uint32_t datagram_len) {

    if (len - (*pos + 4 + 8) <= 1) {
        std::cerr << "datagram can not contains player names\n";
        *flag = false;
        return "";
    }

    std::stringstream result;
    std::string delim;
    char *iter = datagram_start;
    std::string temp_name;
    while (iter != datagram_start + datagram_len && *pos < len) {
        if (*iter == '\0') {
            if (temp_name.length() > 0) {
                players_names.emplace_back(temp_name);
                result << delim << temp_name;
                delim = " ";
                temp_name.clear();
            }
        } else {
            temp_name += *iter;
        }
        ++iter;
        *pos += 1;
    }


    return result.str();
}

bool client::check_event_no(uint32_t new_event_no) const {
    return (new_event_no == next_expected_event_no || new_event_no == 0);
}


void client::handle_mess_from_gui() {
    char buffer[max_mess_from_server_size + 2];

    ssize_t len;
    int flags;

    flags = 0;
    len = recv(gui_socket, buffer, sizeof(buffer),
               flags);

    if (len < 0) {
        if (errno == 111) {
            std::cerr <<  "gui lost connection\n"; // check errno
            exit(1);
        } else {
            std::cerr
                    << "error on datagram from gui socket\n";
        }
    }
    ssize_t pos = 0;
    std::string str;
    while (pos < len) {
        if (buffer[pos] == '\n') {
            if (str.length() > 0) {
                if (str == "LEFT_KEY_DOWN") {
                    turn_direction = 2;
                } else if (str == "RIGHT_KEY_DOWN") {
                    turn_direction = 1;
                } else if (str == "LEFT_KEY_UP" || str == "RIGHT_KEY_UP") {
                    turn_direction = 0;
                }

                str.clear();
            }
        } else if (buffer[pos] != '\0') {
            str += buffer[pos];
        }
        pos += 1;
    }

}



void client::try_send_to_server() {
    if (client_poll[0].revents & POLLOUT && !server_mess_history.empty()) {
        ssize_t mess_len = (ssize_t ) server_mess_history.front()->get_len();
        ssize_t len = write(server_socket, server_mess_history.front()->get_buffer(), mess_len);
        if(len != mess_len){
            std::cerr<<"not whole datagram sent to server\n";
            exit(1);
        }
        server_mess_history.pop();
        client_poll[0].revents &= ~POLLOUT;
        if (server_mess_history.empty())
            client_poll[0].events &= ~POLLOUT;
    }
}

void client::try_send_to_gui() {
    if (client_poll[1].revents & POLLOUT) {
      //  std::cout << gui_mess_history.front().c_str() << std::endl;
        ssize_t len = write(gui_socket, gui_mess_history.front().c_str(),gui_mess_history.front().length());
        if(len != (ssize_t)gui_mess_history.front().length()){
            std::cerr<<"not whole datagram sent to gui\n";
            exit(1);
        }
        gui_mess_history.pop();
        client_poll[1].revents &= ~POLLOUT;
        if (gui_mess_history.empty())
            client_poll[1].events &= ~POLLOUT;
    }
}

void client::create_mess_to_send_to_server() {
    uint64_t micro_seconds_left;
    if (!wes_serv_informed) {
        micro_seconds_left = 40 * 1000;
        wes_serv_informed = true;
    } else {
        auto now = std::chrono::high_resolution_clock::now();
        auto elapsed = now - last_mess_to_server;
        micro_seconds_left = std::chrono::duration_cast<std::chrono::microseconds>(
                elapsed).count();

    }

    if (micro_seconds_left >= 30 * 1000) {
        ClientToServerMess mess_to_send;
        mess_to_send.setSessionId(session_id);
        mess_to_send.setNextExpectedEventNo(next_expected_event_no);
        mess_to_send.setTurnDirection(turn_direction);
        mess_to_send.setPlayerName(player_name);
        mess_to_send.create_sender();


        auto mess_to_server = std::make_shared<ClientToServerMessHandler>(0);
        mess_to_server->create_mess(mess_to_send);
        server_mess_history.push(mess_to_server);
        client_poll[0].events |= POLLOUT;
        last_mess_to_server = std::chrono::high_resolution_clock::now();
    }
}






