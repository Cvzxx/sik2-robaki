//
// Created by pawel on 24.08.2021.
//


#include "Game.h"

bool Game::is_session_live()  {
    return session_live;
}

size_t Game::num_players_ready() {
    size_t counter = 0;

    for (const auto &player: players) {
        if (player->isReady())
            counter++;
    }

    return counter;
}


Game::Game(uint32_t turningSpeed, uint32_t roundsPerSec, uint32_t boardWidth,
           uint32_t boardHeigth, uint32_t seed) : turning_speed(turningSpeed),
                                                  rounds_per_sec(roundsPerSec),
                                                  board_width(boardWidth),
                                                  board_height(boardHeigth) {
    my_gen.setSeed(seed);
    session_live = false;
}



bool Game::handle_new_player(std::shared_ptr<player> new_player){
    if(new_player->getPlayerName().length() > 0){
        if(!new_player->isReady() && new_player->getTurnDirection() != 0) {
            new_player->setIsReady(true);

        }

        players.emplace_back(new_player);
        return true;
    }else{
        spectators.emplace_back(new_player);
        return true;
    }
}
bool Game::handle_existing_player(std::shared_ptr<player> ext_player,
                                  ClientToServerMess mess) {
    if (ext_player->getSessionId() <= mess.getSessionId()) {

        ext_player->setSessionId(mess.getSessionId());
   //     std::cout << "WTF\n";
        ext_player->setTurnDirection(mess.getTurnDirection());
        if(mess.getTurnDirection() != 0)
            ext_player->setLastNotZero(mess.getTurnDirection());
        ext_player->setNextExpectedEventNo(mess.getNextExpectedEventNo());
        if (!ext_player->isReady() &&
            mess.getTurnDirection() != 0) { //if game is not in progress?
            ext_player->setIsReady(true);
        }

        return true;
    }

    //ignore
    return false;
}

void Game::erase_inactive_players(std::string rem_name_player) {
    auto it = players.begin();

    while (it != players.end()) {
        if ((*it)->getPlayerName() == rem_name_player) {
            (*it)->setIsDisconnected(true);
            it = players.erase(it);
        } else {
            ++it;
        }
    }

    it = spectators.begin();
    while (it != spectators.end()) {
        if ((*it)->getPlayerName() == rem_name_player) {
            it = spectators.erase(it);
        } else {
            ++it;
        }
    }
}

uint32_t Game::getGameId() const {
    return game_id;
}

void Game::game_rand() {
    game_id = my_gen.my_rand();
}

uint32_t Game::get_rand() {
    return my_gen.my_rand();
}

uint32_t Game::getBoardWidth() const {
    return board_width;
}

uint32_t Game::getBoardHeigth() const {
    return board_height;
}

void Game::sort_player() {
    std::sort(players.begin(), players.end(),
              [](const std::shared_ptr<player> &lhs, const std::shared_ptr<player> &rhs) {
                  return lhs->getPlayerName() < rhs->getPlayerName();
              });
}

void Game::find_active_players() {

    sort_player();
    for (const auto &player: players) {
        if (player->isReady() && !player->isDisconnected()) {
            active_players.emplace_back(player);
        }
    }
}

bool Game::check_cords_range(uint32_t x, uint32_t y) {

    if (x < board_width && y < board_height)
        return true;

    return false;
}


bool Game::check_pixel(double x, double y) {
    if(x < 0 || y < 0)
        return false;

    auto temp_x = static_cast <uint32_t> (std::floor(x));
    auto temp_y = static_cast <uint32_t> (std::floor(y));

    if (check_cords_range(temp_x, temp_y) && board[temp_y][temp_x] == 0)
        return true;

    return false;
}


void Game::eat_pixel(double x, double y) {
    auto temp_x = static_cast <uint32_t> (std::floor(x));
    auto temp_y = static_cast <uint32_t> (std::floor(y));

    board[temp_y][temp_x] = 1;
}

std::vector<std::shared_ptr<event>> Game::init_players(size_t nums) {
    std::vector<std::shared_ptr<event>> events;
    char player_no = 0;
    size_t counter = nums;
    for (auto &player: active_players) {
        player->setX((get_rand() % board_width) + 0.5);
        player->setY((get_rand() % board_height) + 0.5);
        player->setCurrentDirection(get_rand() % 360);

        if (check_pixel(player->getX(), player->getY())) {
            eat_pixel(player->getX(), player->getY());
            player->setIsAlive(true);
            alive_players++;
            auto pixel_event = std::make_shared<event>(0);
            pixel_event->create_pixel_event(counter, player_no, player->getX(),
                                            player->getY());
            events.emplace_back(pixel_event);
        } else {
            player->setIsAlive(false);
            auto player_eliminated_event = std::make_shared<event>(0);
            player_eliminated_event->create_player_eliminated_event(counter,
                                                                    player_no);
            events.emplace_back(player_eliminated_event);
        }
        ++counter;
        ++player_no;
    }

    if (alive_players < 2) {
        std::cout << "GAME_START WAS THERE\n";
        alive_players = 0;
        session_live = false;
        make_all_unready();
        auto game_over_event = std::make_shared<event>(0);
        game_over_event->create_game_over_event(counter);
        events.emplace_back(game_over_event);
    }

    return events;
}

void Game::clear_board() {
    std::vector<std::vector<uint32_t>> vec(board_height,
                                           std::vector<uint32_t>(board_width,
                                                                 0));
    board = vec;
}


void Game::new_game() {
    session_live = true;
    alive_players = 0;
    game_id = get_rand();
    clear_board();
    active_players.clear();
    find_active_players();
}


void Game::make_all_unready() {
    for (auto &player: players) {
        player->setIsReady(false);
    }
}

std::vector<std::shared_ptr<event>> Game::play_round(size_t nums) {
    std::vector<std::shared_ptr<event>> events;
    char player_no = 0;

    size_t counter = nums;
    for (auto &player: active_players) {
        if (!player->isAlive()) {
            ++player_no;
            continue;
        }

        if (player->getTurnDirection() == 1) {
            int temp_dir = player->getCurrentDirection() +
                           static_cast <int>(turning_speed);
            temp_dir %= 360;
            player->setCurrentDirection(temp_dir);
        } else if (player->getTurnDirection() == 2) {
            int temp_dir = player->getCurrentDirection() -
                           static_cast <int>(turning_speed);
            temp_dir %= 360;
            player->setCurrentDirection(temp_dir);
        }

        std::pair<double, double> new_cords = player->move_player_one_pixel();

        if (player->cmp_cords(new_cords.first, new_cords.second)) {
            ++player_no;
           // std::cout << (int) player_no << "NOT MOVING turn " << (int) player->getTurnDirection() << "\n";
            continue;
        }

        if (check_pixel(new_cords.first, new_cords.second)) {
            player->setX(new_cords.first);
            player->setY(new_cords.second);
            eat_pixel(player->getX(), player->getY());
            auto pixel_event = std::make_shared<event>(0);
            pixel_event->create_pixel_event(counter, player_no, player->getX(),
                                            player->getY());
            events.emplace_back(pixel_event);
        } else {
            --alive_players;
            player->setIsAlive(false);
            auto player_eliminated_event = std::make_shared<event>(0);
            player_eliminated_event->create_player_eliminated_event(counter,
                                                                    player_no);
            events.emplace_back(player_eliminated_event);
        }

        ++counter;
        if (alive_players < 2) {
            std::cout << "WAS THERE\n";
            alive_players = 0;
            session_live = false;
            make_all_unready();
            auto game_over_event = std::make_shared<event>(0);
            game_over_event->create_game_over_event(counter);
            events.emplace_back(game_over_event);
            break;
        }

        ++player_no;
    }

    return events;
}

std::vector<std::string> Game::get_active_players_names() {
    std::vector<std::string> temp;

    for (const auto &player: active_players) {
        temp.emplace_back(player->getPlayerName());
    }

    return temp;
}

uint32_t Game::getAlivePlayers() const {
    return alive_players;
}

size_t Game::players_size() {
    return players.size();
}

void Game::set_last_round(
        std::chrono::high_resolution_clock::time_point last_round_time) {
    last_round = last_round_time;
}

std::chrono::high_resolution_clock::time_point Game::get_last_round() {
    return last_round;
}

uint32_t Game::getRoundsPerSec() const {
    return rounds_per_sec;
}














