//
// Created by pawel on 22.08.2021.
//

#include "server.h"

void server::reset_poll() {
    for (auto &i: server_poll) {
        i.fd = -1;
        i.events = POLLIN | POLLERR;
        i.revents = 0;
    }
}

void server::set_poll() {
    server_poll[0].fd = listening_socket;
}

void server::create_poll() {
    reset_poll();
    set_poll();
}

void server::set_server() {
    listening_socket = socket(AF_INET6, SOCK_DGRAM, 0);

    if (listening_socket < 0) {
        std::cerr << "socket server\n";
        exit(1);
    }

    server_address.sin6_family = AF_INET6;
    server_address.sin6_addr = in6addr_any;
    server_address.sin6_port = htobe16(port_num);

    if (bind(listening_socket, (struct sockaddr *) &server_address,
             sizeof(server_address))) {
        std::cerr << "bind server\n";
        exit(1);
    }

    create_poll();
}

server::server(uint32_t port_num) {
    this->port_num = port_num;
}


void server::set_game(uint32_t turningSpeed, uint32_t roundsPerSec,
                      uint32_t boardWidth,
                      uint32_t boardHeight, uint32_t seed) {

    game_state = std::make_shared<Game>(turningSpeed, roundsPerSec, boardWidth,
                                        boardHeight, seed);
}

void server::check_active_players() {
    auto now = std::chrono::high_resolution_clock::now();
    auto elapsed = now - last_checkout;
    uint64_t micro_seconds_left = std::chrono::duration_cast<std::chrono::microseconds>(
            elapsed).count();

    if (micro_seconds_left >= checkout_time) {
        for (auto it = clients.begin(); it != clients.end();) {
            auto last_mess_time_elapsed = now - it->second->getLastMess();
            uint64_t micro_seconds_last_mess = std::chrono::duration_cast<std::chrono::microseconds>(
                    last_mess_time_elapsed).count();
            if (micro_seconds_last_mess >= 2 * 1000 * 1000) {
                game_state->erase_inactive_players(it->second->getPlayerName());
                it = clients.erase(it);
            } else {
                ++it;
            }
        }

        last_checkout = std::chrono::high_resolution_clock::now();
    }

}

void server::new_session() {
    events_history.clear();
    game_state->new_game();
   // game_state->game_rand();

    auto new_game_event = std::make_shared<event>(0);
    new_game_event->create_new_game_event(events_history.size(),
                                          game_state->getBoardWidth(),
                                          game_state->getBoardHeigth(),
                                          game_state->get_active_players_names());
    events_history.emplace_back(new_game_event);
    send_event_to_all( game_state->getGameId());
    std::vector<std::shared_ptr<event>> temp_events = game_state->init_players(events_history.size());

    size_t prev_events_size = events_history.size();
    events_history.insert(events_history.end(), temp_events.begin(),
                          temp_events.end());
    if (events_history.size() > prev_events_size)
        send_to_all();
}

void server::try_start_game() {
    size_t num_players = game_state->num_players_ready();
    if (!game_state->is_session_live() && num_players > 1 &&
        num_players == game_state->players_size()) {
    //    std::cout << "CREATE NEW SESSION\n";
        new_session();
    }
}

void server::new_round() {
    std::vector<std::shared_ptr<event>> temp_events = game_state->play_round(events_history.size());
  //  std::cout << std::boolalpha << game_state->is_session_live() << std::endl;
    size_t prev_events_size = events_history.size();

    events_history.insert(events_history.end(), temp_events.begin(),
                          temp_events.end());
    if (events_history.size() > prev_events_size)
        send_to_all();
}

void server::try_start_round() {
    auto elapsed = std::chrono::high_resolution_clock::now() -
                   game_state->get_last_round();
    uint64_t micro_seconds_left = std::chrono::duration_cast<std::chrono::microseconds>(
            elapsed).count();
    uint64_t round_time = 1000 * 1000 / game_state->getRoundsPerSec();
    if (micro_seconds_left >= round_time && game_state->is_session_live() &&
        game_state->getAlivePlayers() > 1) {
        //std::cout << "CREATE NEW ROUND\n";
        new_round();
        game_state->set_last_round(std::chrono::high_resolution_clock::now());
    }

}


void server::run() {
    bool is_running = true;
    int ret;
    std::cout << "RUN\n";
    do {

        ret = poll(server_poll, 1, 1); // do poprawy
        if (ret == -1) {
            std::cerr << "server_poll poll() err\n";
            exit(1);
        } else {

            if (server_poll[0].revents & (POLLIN | POLLERR)) {


                if (handle_mess_from_client()) {
                    try_start_game();

                }
                server_poll[0].revents &= ~(POLLIN | POLLERR);
            }
            if (server_poll[0].revents & POLLOUT) {
                handle_mess_to_client();
            }
        }


        try_start_round();
        check_active_players();
        //is_running = false;
    } while (is_running);
}


void server::handle_mess_to_client() {

    int sflags = 0;
    auto infos = events_to_send.front();
    uint32_t mess_len = infos.second->get_len();
    ssize_t snd_len = sendto(server_poll[0].fd, infos.second->get_buffer(),
                             mess_len, sflags,
                             (sockaddr *) infos.first.get_addrr(),
                             infos.first.len());

    events_to_send.pop();
    server_poll[0].revents &= ~POLLOUT;
    if (events_to_send.empty())
        server_poll[0].events &= ~POLLOUT;

    if (snd_len != mess_len)
        std::cerr << "error on sending datagram to client socket\n";
}

void server::send_to_all() {
    for (auto &client: clients) {
        send_events(client.first, client.second->getNextExpectedEventNo(),
                    game_state->getGameId());
    }
}

void server::send_event_to_all(uint32_t game_id) {
    for (auto &client: clients) {
        send_events(client.first, 0, game_id);
    }
}

void server::send_events(const MySocketAddr &player_address,
                         uint32_t next_expected_event_no, uint32_t game_id) {

    const size_t MAX_MESS_SIZE = 550;
    char buffer[MAX_MESS_SIZE];
    size_t len;
    if (next_expected_event_no < events_history.size()) {
        uint32_t i = next_expected_event_no;
        while (true) {
            len = 0;
            uint32_t temp = htobe32(game_id);
            memcpy(&buffer[len], &temp, sizeof(temp));
            len += sizeof(temp);
            for (; i < events_history.size(); ++i) {
                if (events_history[i]->getLen() + len < MAX_MESS_SIZE) {
                    char *temp_event_data = events_history[i]->get_buffer();
                    memcpy(&buffer[len], temp_event_data,
                           events_history[i]->getLen());
                    len += events_history[i]->getLen();
                } else {
                    break;
                }
            }

            auto mess_to_send = std::make_shared<ServerToClientMessHandler>(0);
            mess_to_send->create_mess(buffer, len);
            events_to_send.push(std::make_pair(player_address, mess_to_send));
            server_poll[0].events |= POLLOUT;
            if (i >= events_history.size()) {
                break;
            }
        }

    } else {
        return;
    }

}


bool server::handle_mess_from_client() {
    MySocketAddr client_address;
    char buffer[max_client_mess_len + 2];
    std::fill(buffer, buffer + max_client_mess_len + 2, '\0');

    ssize_t len;
    int flags;

    flags = 0;
    len = recvfrom(server_poll[0].fd, buffer, max_client_mess_len + 1, flags,
                   (sockaddr *) client_address.get_addrr(),
                   client_address.len_ptr());

  //  std::cout << "ads" << " " << len << std::endl;
    if (len < 0) {
        std::cerr << "error on datagram from client socket\n";
        return false;
    } else {
        if (len >=13) {
            //valid mess
            ClientToServerMess mess;
            if (!mess.create_parser(buffer, len)) {
                std::cerr << "problem with datagram\n";
                return false;
            }
            auto client_pos = clients.find(client_address);
            auto new_player = std::make_shared<player>(mess.getSessionId(),
                                                       mess.getTurnDirection(),
                                                       mess.getNextExpectedEventNo(),
                                                       mess.getPlayerName());

           // std::cout << mess.getSessionId() << " " << (int) mess.getTurnDirection() << mess.getNextExpectedEventNo() << " " << mess.getPlayerName() << std::endl;
            //validate player, if not then skip, change passing player to game
            if (!new_player->is_valid()) {
                std::cerr << "invalid player infos from datagram\n";
                return false;
            }
            new_player->setLastMess(std::chrono::high_resolution_clock::now());

            if (client_pos == clients.end()) {
                if (game_state->handle_new_player(new_player)) {
                    clients[client_address] = new_player;
                    send_events(client_address, mess.getNextExpectedEventNo(),
                                game_state->getGameId());
                    //send mess
                }
            } else {
                if (new_player->getPlayerName() ==
                    client_pos->second->getPlayerName()) {
                    if (game_state->handle_existing_player(client_pos->second,
                                                           mess)) {
                        send_events(client_address,
                                    mess.getNextExpectedEventNo(),
                                    game_state->getGameId());
                        //send mess
                    }
                }
            }
        }

        return true;
    }
}













