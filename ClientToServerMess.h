//
// Created by pawel on 23.08.2021.
//

#ifndef SIK2_CLIENTTOSERVERMESS_H
#define SIK2_CLIENTTOSERVERMESS_H

#include <iostream>
#include <cstring>
class ServerToClientMessHandler{
public:
    explicit ServerToClientMessHandler(uint32_t len);
    void create_mess( char *mess, uint32_t mess_size);
    char *get_buffer();
    uint32_t get_len();
private:
    char buffer[550];
    uint32_t len;
};



class ClientToServerMess {
private:
    uint64_t session_id;
    char turn_direction;
    uint32_t next_expected_event_no;
    std::string player_name;


    //try to create_parser mess client

    void code_session_id();
    void code_next_expected_event_no();

    //try to read mess from client
    bool encode_session_id(char *buffer, ssize_t len, ssize_t* pos);
    bool encode_turn_direction(char *buffer, ssize_t len, ssize_t *pos);
    bool encode_next_expected_event_no(char *buffer, ssize_t len, ssize_t* pos);
    bool encode_player_name(char *buffer, ssize_t len, ssize_t *pos);
public:


    bool create_parser(char *buffer, ssize_t len);
    void create_sender();

    uint64_t getSessionId() const;
    char getTurnDirection() const;
    uint32_t getNextExpectedEventNo() const;
    const std::string &getPlayerName() const;

    void setSessionId(uint64_t sessionId);

    void setTurnDirection(char turnDirection);

    void setNextExpectedEventNo(uint32_t nextExpectedEventNo);

    void setPlayerName(const std::string &playerName);
};

class ClientToServerMessHandler{
public:
    explicit ClientToServerMessHandler(uint32_t len);
    void create_mess(ClientToServerMess mess);
    char *get_buffer();
    uint32_t get_len();
private:
private:
    char buffer[8+1+4+20+1];
    uint32_t len;
};
#endif //SIK2_CLIENTTOSERVERMESS_H
