
CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra -Werror -O2 

.PHONY: all clean
all: screen-worms-client screen-worms-server


client.o: client.cpp client.h MySocketAddr.h ClientToServerMess.o
screen-worms-client.o: screen-worms-client.cpp client.h
screen-worms-server: screen-worms-server.o server.o Game.o player.o MyGenerator.o ClientToServerMess.o event.o MySocketAddr.o
	$(CXX) $(CXXFLAGS) screen-worms-server.o server.o Game.o player.o MyGenerator.o ClientToServerMess.o event.o MySocketAddr.o -lz -o screen-worms-server
MySocketAddr.o: MySocketAddr.cpp MySocketAddr.h
event.o: event.cpp event.h
ClientToServerMess.o: ClientToServerMess.cpp ClientToServerMess.h
MyGenerator.o: MyGenerator.cpp MyGenerator.h
player.o: player.cpp player.h
Game.o: Game.cpp Game.h player.h MyGenerator.h ClientToServerMess.h event.h
server.o: server.cpp server.h Game.h MySocketAddr.h
screen-worms-server.o: screen-worms-server.cpp server.h
screen-worms-client:  screen-worms-client.o client.o MySocketAddr.o ClientToServerMess.o 
	$(CXX) $(CXXFLAGS) screen-worms-client.o client.o MySocketAddr.o ClientToServerMess.o -lz -o screen-worms-client 
.PHONY: clean
clean:
	rm -rf *.o screen-worms-server screen-worms-client game
